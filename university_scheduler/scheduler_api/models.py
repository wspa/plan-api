from django.db import models


class Lesson(models.Model):
    id = models.AutoField(primary_key=True)
    time_unit = models.DateTimeField()
    teacher = models.CharField(max_length=50)
    classroom = models.PositiveSmallIntegerField()
    subject = models.CharField(max_length=100)
    student_group = models.CharField(max_length=100)
    comment = models.TextField(blank=True)
