from rest_framework import serializers
from scheduler_api.models import Lesson


class LessonSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:

        model = Lesson
        fields = ('id', 'time_unit', 'teacher', 'classroom', 'subject', 'student_group', 'comment')
