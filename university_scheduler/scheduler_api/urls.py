from django.urls import path, include
from rest_framework.routers import DefaultRouter
from scheduler_api import views

router = DefaultRouter()
router.register(r'scheduler_api', views.LessonViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
