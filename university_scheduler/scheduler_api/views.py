from rest_framework import viewsets
from rest_framework import permissions
from scheduler_api.models import Lesson
from scheduler_api.serializers import LessonSerializer


class LessonViewSet(viewsets.ModelViewSet):
    """
    This viewset provides 'list', 'create', 'retrieve', 
    'update', 'partial_update' and 'destroy' actions.
    """

    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
